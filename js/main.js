function init(){
  var canvas = document.getElementById("canvas");
	ctx = canvas.getContext("2d");
  ctx.width = 480;
  ctx.height = 360;
  setInterval("tick()",1000/60);
  player = new Player(240,300,2,5);
  var boss = new enemy(240,30,0,10,0,true,300);
  playerBullets = [], enemies = [];
  enemies.push(boss);
  key = new Array();
  gameState = 0;
}

class Player {
  constructor(x, y, speed, size) {
    this.speed = speed;
    this.size = size;
    this.x = x;
    this.y = y;
  }
  update(){
    this.control();
    if(this.hitCheck()) gameState = -1
    this.draw();
    //console.log("sex")
  }

  control(){
    if(KeyDown(37)) {
      this.x -= this.speed;
    }
    if(KeyDown(38)) {
      this.y -= this.speed;
    }
    if(KeyDown(39)) {
      this.x += this.speed;
    }
    if(KeyDown(40)) {
      this.y += this.speed;
    }
    if(KeyDown(90)) {
      var bullet = new playerBullet(this.x, this.y, 10, 3);
      playerBullets.push(bullet);
    }
  }

  draw(){
    ctx.strokeStyle = "#C0C0C0";
    ctx.lineWidth = 5;
    ctx.fillStyle = "#909090";
    ctx.beginPath();
    ctx.arc(this.x,this.y,this.size, 0,2*Math.PI);
    ctx.stroke();
    ctx.fill();
  }
  hitCheck(){
    for(var i = 0; i<enemies.length; i++){
      if(this.size+enemies[i].size > distance(this.x,this.y,enemies[i].x,enemies[i].y)) {
        return true
      }
    }
    return false
  }
}

class playerBullet {
  constructor(x, y, speed, size) {
    this.speed = speed;
    this.size = size;
    this.x = x;
    this.y = y;
  }
  update(){
    this.control();
    this.draw();
    return outOfScreen(this.x, this.y, 50)
    //console.log("sex")
  }
  control(){
    this.y -= this.speed;
  }
  draw(){
    ctx.strokeStyle = "#C0C0C0";
    ctx.lineWidth = 5;
    ctx.fillStyle = "#909090";
    ctx.beginPath();
    ctx.arc(this.x,this.y,this.size, 0,2*Math.PI);
    ctx.stroke();
    ctx.fill();
  }
}

class enemy {
  constructor(x, y, speed, size, direction, boss, hp) {
    this.speed = speed;
    this.size = size;
    this.x = x;
    this.y = y;
    this.boss = boss;
    this.direction = direction;
    if(boss){
      this.time = 0;
      this.hpAtFirst = hp;
      this.hp = hp;
    }

  }
  update(){
    this.move();
    this.draw();
    if(this.boss){
      this.time++;
      if(this.time > 15){
        var direction = Math.random()*360;
        var n = Math.round((Math.random() + 0.5) * 20)
        for(var i = 0; i<n; i++){
          var bullet = new enemy(this.x, this.y, 3, 5, direction, false);
          enemies.push(bullet);
          direction += 360/n
        }
        this.time = 0
      }

      if(this.hitCheck()) this.hp -= 1;
      if(0 > this.hp) return true
    }
    return outOfScreen(this.x, this.y, 50)

  }
  draw(){
    ctx.strokeStyle = "#C0C0C0";
    ctx.lineWidth = 5;
    ctx.fillStyle = "#900000";
    ctx.beginPath();
    ctx.arc(this.x,this.y,this.size, 0,2*Math.PI);
    ctx.stroke();
    ctx.fill();
    //ctx.closePath();

    if(this.boss) {
      ctx.lineWidth = 5;
      ctx.beginPath();
      ctx.moveTo(20,10);
      ctx.lineTo(460,10);
      ctx.stroke();

      ctx.lineWidth = 2;
      ctx.strokeStyle = "#900000"
      ctx.beginPath();
      ctx.moveTo(21,10);
      ctx.lineTo(21+438*this.hp/this.hpAtFirst,10);
      ctx.stroke();
      ctx.closePath();
    }
  }
  hitCheck(){
    for(var i = 0; i<playerBullets.length; i++){
      if(this.size+playerBullets[i].size > distance(this.x,this.y,playerBullets[i].x,playerBullets[i].y)) {
        return true
      }
    }
    return false
  }
  move(){
    this.x += Math.cos(this.direction*Math.PI/180)*this.speed
    this.y += Math.sin(this.direction*Math.PI/180)*this.speed
  }

}

function tick(){
  ctx.clearRect(0,0,ctx.width,ctx.height);
  if(gameState != -1) {
    player.update();
    for(var i = 0; i<playerBullets.length; i++){
      var destroy=playerBullets[i].update()
      if (destroy){
        playerBullets.splice(i, 1);
        i -= 1;
      }
    }
  }
  for(var i = 0; i<enemies.length; i++){
    var destroy=enemies[i].update()
    if (destroy){
      enemies.splice(i, 1);
      i -= 1;
    }
  }
}

function outOfScreen(x,y,margin){
  if(0 - margin < x && x < ctx.width + margin){
    if(0 - margin < y && y < ctx.height + margin){
      return false;
    }
  }

  return true;
}

function distance(x1,y1,x2,y2){
  return Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
}
